#!/usr/bin/make -f

# dependencies:

# apt install weasyprint xml2rfc ruby-kramdown-rfc2629

draft = external-secrets
OUTPUT = $(draft).txt $(draft).html $(draft).xml $(draft).pdf

all: $(OUTPUT)

%.xml: $(draft).md
	kramdown-rfc --v3 < $< > $@.tmp
	mv $@.tmp $@

%.html: %.xml
	xml2rfc $< --html -o $@

%.txt: %.xml
	xml2rfc $< --text -o $@

%.pdf: %.xml
	xml2rfc $< --pdf -o $@

clean:
	-rm -rf $(OUTPUT)

check:
	codespell --ignore-words .ignore-words $(draft).md
.PHONY: clean all check
