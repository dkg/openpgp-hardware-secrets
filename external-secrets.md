---
title: OpenPGP External Secret Keys
docname: draft-dkg-openpgp-external-secrets-02
category: info

ipr: trust200902
area: int
workgroup: openpgp
keyword: Internet-Draft
submissionType: IETF

stand_alone: yes
pi: [toc, sortrefs, symrefs]

author:
 -
    ins: D. K. Gillmor
    name: Daniel Kahn Gillmor
    org: American Civil Liberties Union
    street: 125 Broad St.
    city: New York, NY
    code: 10004
    country: USA
    abbrev: ACLU
    email: dkg@fifthhorseman.net
venue:
  group: "OpenPGP"
  type: "Working Group"
  mail: "openpgp@ietf.org"
  arch: "https://mailarchive.ietf.org/arch/browse/openpgp/"
  repo: "https://gitlab.com/dkg/openpgp-external-secrets/"
  latest: "https://dkg.gitlab.io/openpgp-external-secrets/"
informative:
 OPENPGP-SMARTCARD:
   target: https://gnupg.org/ftp/specs/OpenPGP-smart-card-application-3.4.1.pdf
   title: Functional Specification of the OpenPGP application on ISO Smart Card Operating Systems, Version 3.4.1
   date: 2020-03-18
   author:
    -
     name: Achim Pietig
 GNUPG-SECRET-STUB:
   target: "https://dev.gnupg.org/source/gnupg/browse/master/doc/DETAILS;gnupg-2.4.3$1511"
   title: GNU Extensions to the S2K algorithm
   date: 2023-07-04
   author:
    -
     name: Werner Koch
     org: g10 Code
 TPM:
   target: "https://trustedcomputinggroup.org/resource/tpm-library-specification/"
   title: "Trusted Platform Module Library Specification, Family “2.0”, Level 00, Revision 01.59"
   date: November 2019
   author:
    -
     org: Trusted Computing Group
 SMART-CARD-FAULTS:
   target: "http://hdl.handle.net/2117/99293"
   title: Smart Card Fault Injections with High Temperatures
   date: 2016-11-15
   author:
    -
     name: Pedro Maat C. Massolino
    -
     name: Baris Ege
    -
     name: Lejla Batina
   
--- abstract

This document defines a standard wire format for indicating that the secret component of an OpenPGP asymmetric key is stored externally, for example on a hardware device or other comparable subsystem.

--- middle

# Introduction

Some OpenPGP secret key material is held by hardware devices that permit the user to operate the secret key without divulging it explicitly.
For example, the {{OPENPGP-SMARTCARD}} specification is intended specifically for this use.
It may also possible for OpenPGP implementations to use external secret key material via standard platform library interfaces like {{TPM}}.

An OpenPGP Secret Key Packet (see {{Section 5.5.3 of !RFC9580}}) is typically used as part of a Transferable Secret Key ({{Section 10.2 of !RFC9580}}) for interoperability between OpenPGP implementations.
An implementation that uses an external secret key needs a standardized way to indicate to another implementation specific secret key material has been delegated to some external mechanism, like a hardware device.

This document defines a simple mechanism for indicating that a secret key has been delegated to an external mechanism by allocating a codepoint in the "Secret Key Encryption (S2K Usage Octet)" registry (see {{Section 3.7.2.1 of RFC9580}}).

It also establishes a registry of hints about how to locate the external device, and defines a minimalist "best effort" method for locating external secret keys that is implementation-specific.

This document makes no attempt to specify how an OpenPGP implementation discovers, enumerates, or operates external secret keys, other than to recommend that the hardware or comparable external subsystem should be identifiable by the secret key's corresponding public key material.

## Requirements Language

{::boilerplate bcp14-tagged}

The key words "PRIVATE USE" and "SPECIFICATION REQUIRED" that appear in this document when used to describe namespace allocation are to be interpreted as described in {{!RFC8126}}.

## Terminology

"Secret key" refers to a single cryptographic object, for example the "56 octets of the native secret key" of X448, as described in {{Section 5.5.5.8 of RFC9580}}.

"Public key" likewise refers to a single cryptographic object, for example the "56 octets of the native public key" of X448, as above.

"OpenPGP certificate" or just "certificate" refers to an OpenPGP Transferable Public Key (see {{Section 10.1 of RFC9580}}).

"External" refers to any cryptographic device or subsystem capable of performing an asymmetric secret key operation using an embedded secret key without divulging the secret to the user.
For discoverability, the external mechanism is also expected to be able to produce the public key corresponding to the embedded secret key.

While this document talks about "external" in the abstract as referring to a cryptographic device embedding a single secret key, most actual hardware devices or other cryptographic subsystems will embed and enable the use of multiple secret keys (see {{multiple-key-hardware}}).

This document uses the term "authorization" to mean any step, such as providing a PIN, password, proof of biometric identity, button-pushing, etc, that the external subsystem may require for an action.

# Externally-backed Secret Key Material {#spec}

An OpenPGP Secret Key packet ({{Section 5.5.3 of RFC9580}}) indicates that the secret key material is stored in an cryptographic subsystem that is identifiable by public key parameters in the following way.

The S2K usage octet is set to TBD (252?), known in shorthand as `External`.

The remainder of the Secret Key packet contains a hint about how the implementation might be able to identify the external subsystem.
This hint sequence is entirely advisory.

If the hint is empty (that is, if there are no bytes in the Secret Key packet following the `External` S2K usage octet, then the hint sequence is known as "best effort" (see {{best-effort}}).

If the hint is not empty, then the first octet of the hint describes the structure of the remainder of the hint, according to the "OpenPGP External Secret Key Locator Hints" registry established by this document.
That registry is initially empty, with octet values 249-255 (inclusive) reserved for PRIVATE USE.
Adding a new entry into that registry in the range 0-248 (inclusive)  uses IANA policy SPECIFICATION REQUIRED.

Regardless of what the locator hint says, a consuming implementation MAY use the "best effort" approach to identify an external subsystem that can provide access to the secret key.

A producing implementation that does not know how to provide a meaningful locator hint SHOULD NOT include any trailing data in the rest of such a Secret Key packet.

A consuming implementation that does not understand any particular locator hint SHOULD ignore any trailing data in such a Secret Key packet.

The purpose of the hinting mechanism is to enable optimized access.
For example, if an OpenPGP implementation has access to several dozen hardware tokens, and if querying each attached hardware token is expensive, a hint can be used to preferentially access a likely token, without probing each token.

This document does not describe any particular hinting scheme.

## Best Effort Access to External Secret Keys {#best-effort}

When no external locator hint is available, or when a consuming implementation does not understand a given locator hint, a consuming implementation uses a "best effort" strategy to identify the external subsystem that provides access to a a secret key that matches the corresponding public key material.

Each OpenPGP implementation might support different external subsystems.
And, in some installations, the same external subsystem might be identified in different ways (for example, a USB smartcard might be connected to hub 2 at low speed, and in another, the same USB smartcard might be connected to hub 1 at high speed.

In some cases, no external subsystem can be identified that supports access to secret key material that corresponds to the associated public key.
Or, the external subsystem might be available, but for whatever reason attempting to use it could fail (for example, the hardware might advertise the availability of the key, but deny access when the implementation tries to use it).
In either case, an OpenPGP implementation that tries to use such an external secret key will fail.
The implementation should fail in a similar way to how it might fail if it tried to use a typical software-backed secret key locked with a password, but the password is unavailable to the implementation.

# Security Considerations

External or hardware-backed secret keys promise several distinct security advantages to the user:

- Often, the secret key cannot be extracted from the external device, so "kleptography" (the stealing of secret key material) is harder to perform.

- Some hardware can be moved between machines, enabling secret key portability without expanding the kleptographic attack surface.

- Some hardware devices offer auditability controls in the form of rate-limiting, user-visible authorization steps (e.g., button-presses or biometric sensors), or tamper-resistant usage counters.
  Malicious use of a secret key on such a device should be harder, or at least more evident.

- Some hardware security devices can attest that key material has been generated on-card, thereby signaling that - barring a successful attack on the hardware - no other copy of the private key material exists.
  Such mechanisms signal that the key holder did not have a chance to mishandle (e.g.: accidentally disclose) the private key material.

However, none of these purported advantages are without caveats.

The hardware itself might actually not resist secret key exfiltration as expected.
For example, isolated hardware devices are sometimes easier to attack physically, via temperature or voltage fluctuations (see {{?VOLTAGE-GLITCHING=DOI.10.48550/arXiv.2108.06131}} and {{SMART-CARD-FAULTS}}).

In some cases, dedicated cryptographic hardware that generates a secret key internally may have significant flaws (see {{?ROCA=DOI.10.1145/3133956.3133969}}).

Furthermore, the most sensitive material in the case of decryption is often the cleartext itself, not the secret key material.
If the host computer itself is potentially compromised, then kleptographic exfiltration of the secret key material itself is only a small risk.
For example, the OpenPGP symmetric session key itself could be exfiltrated, permitting access to the cleartext to anyone without access to the secret key material.

Portability brings with it other risks, including the possibility of abuse by the host software on any of the devices to which the hardware is connected.

Rate-limiting, user-visible authorization steps, and any other form of auditability also suffer from risks related to compromised host operating systems.
Few hardware devices are capable of revealing to the user what operations specifically were performed by the device, so even if the user deliberately uses the device to, say, sign an object, the user depends on the host software to feed the correct object to the device's signing capability.

# Usability Considerations

External secret keys present specific usability challenges for integration with OpenPGP.

## Some Hardware Might Be Unavailable To Some Implementations

This specification gives no hints about how to find the hardware device, and presumes that an implementation will be able to probe available hardware to associate it with the corresponding public key material.
In particular, there is no attempt to identify specific hardware or "slots" using identifiers like PKCS #11 URIs ({{?RFC7512}}) or smartcard serial numbers (see {{historical-notes}}).
This minimalism is deliberate, as it's possible for the same key material to be available on multiple hardware devices, or for a device to be located on one platform with a particular hardware identifier, while on another platform it uses a different hardware identifier.

Not every OpenPGP implementation will be able to talk to every possible hardware device.
If an OpenPGP implementation encounters a hardware-backed secret key as indicated with this mechanism, but cannot identify any attached hardware that lists the corresponding secret key material, it should warn the user that the specific key claims to be hardware-backed but the corresponding hardware cannot be found.
It may also want to inform the user what categories of hardware devices it is capable of probing, for debugging purposes.

## Hardware Should Support Multiple Secret Keys {#multiple-key-hardware}

Most reasonable OpenPGP configurations require the use of multiple secret keys by a single operator.
For example, the user may use one secret key for signing, and another secret key for decryption, and the corresponding public keys of both are contained in the same OpenPGP certificate.

Reasonable hardware SHOULD support embedding and identifying more than one secret key, so that a typical OpenPGP user can rely on a single device for hardware backing.

## Authorization Challenges

Cryptographic hardware can be difficult to use if frequent authorization is required, particularly in circumstances like reading messages in a busy e-mail inbox.
This hardware MAY require authorization for each use of the secret key material as a security measure, but considerations should be made for caching authorization.

If the cryptographic hardware requires authorization for listing the corresponding public key material, it becomes even more difficult to use the device in regular operation.
Hardware SHOULD NOT require authorization for the action of producing the corresponding public key.

If a user has two attached pieces of hardware that both hold the same secret key, and one requires authorization while the other does not, it is reasonable for an implementation to try the one that doesn't require authorization first.
Some cryptographic hardware is designed to lock the device on repeated authorization failures (e.g. 3 bad PIN entries locks the device), so this approach reduces the risk of accidental lockout.

## Latency and Error Handling

While hardware-backed secret key operations can be significantly slower than modern computers, and physical affordances like button-presses or NFC tapping can themselves incur delay, an implementation using a hardware-backed secret key should remain responsive to the user.
It should indicate when some interaction with the hardware may be required, and it should use a sensible timeout if the hardware device appears to be unresponsive.

A reasonable implementation should surface actionable errors or warnings from the hardware to the user where possible.

# IANA Considerations

This document asks IANA to make three changes in the "OpenPGP" protocol group.

Add the following row in the "OpenPGP Secret Key Encrpytion (S2K Usage Octet)" registry:

{: title="Row to add to OpenPGP Secret Key Encrpytion (S2K Usage Octet) registry"}
S2K usage octet | Shorthand | Encryption parameter fields | Encryption | Generate?
-----|----|---|---|---
TBD (252?) | External | External Locator Hint, see {{spec}} of RFC XXX (this document) | no data | Yes

Modify this row of the "OpenPGP Symmetric Key Algorithms" registry:

{: title="Row to modify in OpenPGP Symmetric Key Algorithms registry"}
ID | Algorithm
--:|---
253, 254, and 255 | Reserved to avoid collision with Secret Key Encryption

to include TBD (252?) in this reserved codepoint sequence, resulting in the following entry:

{: title="Modified row in OpenPGP Symmetric Key Algorithms registry"}
ID | Algorithm
--:|---
TBD (252?), 253, 254, and 255 | Reserved to avoid collision with Secret Key Encryption


Establish a new registry, "OpenPGP External Secret Key Locator Hints", with the following columns and initial range:

{: title="OpenPGP External Secret Key Locator Hints"}
ID | Shorthand | Description | Reference
--:|---|---|---
0-191 | Unassigned | | RFC XXX (this document)
249-255 | Private Use | | RFC XXX (this document)

Assigning a new codepoint to this registry uses SPECIFICATION REQUIRED.

--- back

# Historical notes

Some OpenPGP implementations make use of private codepoint ranges in the OpenPGP specification within an OpenPGP Transferable Secret Key to indicate that the secret key can be found on a smartcard.

For example, GnuPG uses the private/experimental codepoint 101 in the S2K Specifier registry, along with an embedded trailer with an additional codepoint, plus the serial number of the smartcard (see {{GNUPG-SECRET-STUB}}).

However, recent versions of that implementation ignore the embedded serial number in favor of scanning available devices for a match of the key material, since some people have multiple cards with the same secret key.


# Test vectors

## Example Transferable Secret Key

The OpenPGP Transferable Secret Key used for this example. It includes (unencrypted) private key material for both its primary key and one subkey:

{: sourcecode-name="software-backed.key"}
~~~ application/pgp-keys
{::include test-vectors/software-backed.key}
~~~

## As an External Secret Key

The same OpenPGP Transferable Secret Key with the S2K Usage Octet set to 252? (External) for both the Primary Key Packet and the Subkey Packet. This format omits all data following the S2K Usage Octet:

{: sourcecode-name="external-secret.key"}
~~~ application/pgp-keys
{::include test-vectors/external-secret.key}
~~~

The (primary) Secret-Key Packet of this key looks as follows, in this format:

~~~
0000  c5                        packet type: Secret-Key Packet
0001     34                     packet length
0002        04                  version
0003           66 05 ad 73      creation time
0007                       16   public-key algorithm: EdDSALegacy
0008  09                        curve OID length
0009     2b 06 01 04 01 da 47   curve OID
0010  0f 01
0012        01 07               EdDSA public key Q MPI length
0014              40 94 b2 ba   EdDSA public key Q MPI
0018  50 f4 2c 54 74 76 11 39
0020  35 4b 05 48 1b 7b 41 9a
0028  98 84 b6 29 18 62 50 b2
0030  d5 32 22 ab 36
0035                 fc         S2K usage octet
~~~

# Acknowledgements

This work depends on a history of significant work with hardware-backed OpenPGP secret key material, including useful implementations and guidance from many people, including:

- NIIBE Yutaka
- Achim Pietig
- Werner Koch
- Heiko Schäfer

The people acknowledeged in this section are not responsible for any proposals, errors, or omissions in this document.

# Document History

## Substantive Changes from draft-dkg-openpgp-external-secrets-00 to draft-dkg-openpgp-external-secrets-01

- define the external locator hinting mechanism

## Substantive Changes from draft-dkg-openpgp-hardware-secrets-02 to draft-dkg-openpgp-external-secrets-00

- rename from "Hardware-backed" to "External"
- use RFC9580 instead of I-D.ietf-openpgp-crypto-refresh

## Substantive Changes from draft-dkg-openpgp-hardware-secrets-01 to draft-dkg-openpgp-hardware-secrets-02

- re-format hexdump of test vector secret key packet

## Substantive Changes from draft-dkg-openpgp-hardware-secrets-00 to draft-dkg-openpgp-hardware-secrets-01

- Added test vector for experimentation
- Mention on-device attestation
- update OpenPGP card spec reference to 3.4.1
