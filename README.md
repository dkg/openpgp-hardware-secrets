# OpenPGP External Secret Key Material


This repository documents an interoperable wire format that indicates that the secret material for an asymmetric OpenPGP key is backed by an external subsystem like a hardware device, such as a smartcard or a TPM.

It is currently published with the IETF at https://datatracker.ietf.org/doc/draft-dkg-openpgp-external-secrets

You might also want to see [the latest editor's copy](https://dkg.gitlab.io/openpgp-external-secrets/).
